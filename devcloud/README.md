# 微服务研发云

基于k8s 来开发:
+ 部署服务:  服务部署(工作负载管理)
+ Pipeline: 任务执行(k8s job), 流程出发(Operator架构)

## Kubernetes 简介与环境搭建

+ Docker Desktop 安装一个k8s (安装)
+ kubeadm
+ minikube
+ 托管集群云商(用完及时释放)

```sh
$ kubectl version 
Client Version: v1.29.2
Kustomize Version: v5.0.4-0.20230601165947-6ce0bf390ce3
Server Version: v1.29.2
```

## client-go使用

服务部署: kubectl create deploy -f deployfile.yaml, 使用client-go来实现相同功能

[](https://kubernetes.io/zh-cn/docs/home/)


### k8s API 资源对象

```sh
kubectl api-resources
NAME                              SHORTNAMES   APIVERSION                        NAMESPACED   KIND
bindings                                       v1                                true         Binding
componentstatuses                 cs           v1                                false        ComponentStatus
configmaps                        cm           v1                                true         ConfigMap
endpoints                         ep           v1                                true         Endpoints
events                            ev           v1                                true         Event
limitranges                       limits       v1                                true         LimitRange
namespaces                        ns           v1                                false        Namespace
nodes                             no           v1                                false        Node
persistentvolumeclaims            pvc          v1                                true         PersistentVolumeClaim
persistentvolumes                 pv           v1                                false        PersistentVolume
pods                              po           v1                                true         Pod
podtemplates                                   v1                                true         PodTemplate
replicationcontrollers            rc           v1                                true         ReplicationController
resourcequotas                    quota        v1                                true         ResourceQuota
secrets                                        v1                                true         Secret
serviceaccounts                   sa           v1                                true         ServiceAccount
services                          svc          v1                                true         Service
mutatingwebhookconfigurations                  admissionregistration.k8s.io/v1   false        MutatingWebhookConfiguration
validatingwebhookconfigurations                admissionregistration.k8s.io/v1   false        ValidatingWebhookConfiguration
customresourcedefinitions         crd,crds     apiextensions.k8s.io/v1           false        CustomResourceDefinition
apiservices                                    apiregistration.k8s.io/v1         false        APIService
controllerrevisions                            apps/v1                           true         ControllerRevision
daemonsets                        ds           apps/v1                           true         DaemonSet
deployments                       deploy       apps/v1                           true         Deployment
replicasets                       rs           apps/v1                           true         ReplicaSet
statefulsets                      sts          apps/v1                           true         StatefulSet
selfsubjectreviews                             authentication.k8s.io/v1          false        SelfSubjectReview
tokenreviews                                   authentication.k8s.io/v1          false        TokenReview
localsubjectaccessreviews                      authorization.k8s.io/v1           true         LocalSubjectAccessReview
selfsubjectaccessreviews                       authorization.k8s.io/v1           false        SelfSubjectAccessReview
selfsubjectrulesreviews                        authorization.k8s.io/v1           false        SelfSubjectRulesReview
subjectaccessreviews                           authorization.k8s.io/v1           false        SubjectAccessReview
horizontalpodautoscalers          hpa          autoscaling/v2                    true         HorizontalPodAutoscaler
cronjobs                          cj           batch/v1                          true         CronJob
jobs                                           batch/v1                          true         Job
certificatesigningrequests        csr          certificates.k8s.io/v1            false        CertificateSigningRequest
leases                                         coordination.k8s.io/v1            true         Lease
endpointslices                                 discovery.k8s.io/v1               true         EndpointSlice
events                            ev           events.k8s.io/v1                  true         Event
flowschemas                                    flowcontrol.apiserver.k8s.io/v1   false        FlowSchema
prioritylevelconfigurations                    flowcontrol.apiserver.k8s.io/v1   false        PriorityLevelConfiguration
ingressclasses                                 networking.k8s.io/v1              false        IngressClass
ingresses                         ing          networking.k8s.io/v1              true         Ingress
networkpolicies                   netpol       networking.k8s.io/v1              true         NetworkPolicy
runtimeclasses                                 node.k8s.io/v1                    false        RuntimeClass
poddisruptionbudgets              pdb          policy/v1                         true         PodDisruptionBudget
clusterrolebindings                            rbac.authorization.k8s.io/v1      false        ClusterRoleBinding
clusterroles                                   rbac.authorization.k8s.io/v1      false        ClusterRole
rolebindings                                   rbac.authorization.k8s.io/v1      true         RoleBinding
roles                                          rbac.authorization.k8s.io/v1      true         Role
priorityclasses                   pc           scheduling.k8s.io/v1              false        PriorityClass
csidrivers                                     storage.k8s.io/v1                 false        CSIDriver
csinodes                                       storage.k8s.io/v1                 false        CSINode
csistoragecapacities                           storage.k8s.io/v1                 true         CSIStorageCapacity
storageclasses                    sc           storage.k8s.io/v1                 false        StorageClass
volumeattachments                              storage.k8s.io/v1                 false        VolumeAttachment
```

### 客户端认证

KubeConfig: 对象的客户端认证: ~/.kube/config 
```yaml
apiVersion: v1
clusters:
- cluster:
    certificate-authority-data: xxx
    server: https://127.0.0.1:6443
  name: docker-desktop
contexts:
- context:
    cluster: docker-desktop
    user: docker-desktop
  name: docker-desktop
current-context: docker-desktop
kind: Config
preferences: {}
users:
- name: docker-desktop
  user:
    client-certificate-data: xxx
    client-key-data: xxx
```

client-go 如何基于 Config对象来进行认证

client-go版本选择的核心: https://github.com/kubernetes/client-go
1. 与服务端匹配
2. 客户端是向后兼容: 高过服务端的版本



### k8s ClientSet

ClientSet 核心接口
```go
type Interface interface {
	Discovery() discovery.DiscoveryInterface
	AdmissionregistrationV1() admissionregistrationv1.AdmissionregistrationV1Interface
	AdmissionregistrationV1alpha1() admissionregistrationv1alpha1.AdmissionregistrationV1alpha1Interface
	AdmissionregistrationV1beta1() admissionregistrationv1beta1.AdmissionregistrationV1beta1Interface
	InternalV1alpha1() internalv1alpha1.InternalV1alpha1Interface
	AppsV1() appsv1.AppsV1Interface
	AppsV1beta1() appsv1beta1.AppsV1beta1Interface
	AppsV1beta2() appsv1beta2.AppsV1beta2Interface
	AuthenticationV1() authenticationv1.AuthenticationV1Interface
	AuthenticationV1alpha1() authenticationv1alpha1.AuthenticationV1alpha1Interface
	AuthenticationV1beta1() authenticationv1beta1.AuthenticationV1beta1Interface
	AuthorizationV1() authorizationv1.AuthorizationV1Interface
	AuthorizationV1beta1() authorizationv1beta1.AuthorizationV1beta1Interface
	AutoscalingV1() autoscalingv1.AutoscalingV1Interface
	AutoscalingV2() autoscalingv2.AutoscalingV2Interface
	AutoscalingV2beta1() autoscalingv2beta1.AutoscalingV2beta1Interface
	AutoscalingV2beta2() autoscalingv2beta2.AutoscalingV2beta2Interface
	BatchV1() batchv1.BatchV1Interface
	BatchV1beta1() batchv1beta1.BatchV1beta1Interface
	CertificatesV1() certificatesv1.CertificatesV1Interface
	CertificatesV1beta1() certificatesv1beta1.CertificatesV1beta1Interface
	CertificatesV1alpha1() certificatesv1alpha1.CertificatesV1alpha1Interface
	CoordinationV1beta1() coordinationv1beta1.CoordinationV1beta1Interface
	CoordinationV1() coordinationv1.CoordinationV1Interface
	CoreV1() corev1.CoreV1Interface
	DiscoveryV1() discoveryv1.DiscoveryV1Interface
	DiscoveryV1beta1() discoveryv1beta1.DiscoveryV1beta1Interface
	EventsV1() eventsv1.EventsV1Interface
	EventsV1beta1() eventsv1beta1.EventsV1beta1Interface
	ExtensionsV1beta1() extensionsv1beta1.ExtensionsV1beta1Interface
	FlowcontrolV1() flowcontrolv1.FlowcontrolV1Interface
	FlowcontrolV1beta1() flowcontrolv1beta1.FlowcontrolV1beta1Interface
	FlowcontrolV1beta2() flowcontrolv1beta2.FlowcontrolV1beta2Interface
	FlowcontrolV1beta3() flowcontrolv1beta3.FlowcontrolV1beta3Interface
	NetworkingV1() networkingv1.NetworkingV1Interface
	NetworkingV1alpha1() networkingv1alpha1.NetworkingV1alpha1Interface
	NetworkingV1beta1() networkingv1beta1.NetworkingV1beta1Interface
	NodeV1() nodev1.NodeV1Interface
	NodeV1alpha1() nodev1alpha1.NodeV1alpha1Interface
	NodeV1beta1() nodev1beta1.NodeV1beta1Interface
	PolicyV1() policyv1.PolicyV1Interface
	PolicyV1beta1() policyv1beta1.PolicyV1beta1Interface
	RbacV1() rbacv1.RbacV1Interface
	RbacV1beta1() rbacv1beta1.RbacV1beta1Interface
	RbacV1alpha1() rbacv1alpha1.RbacV1alpha1Interface
	ResourceV1alpha2() resourcev1alpha2.ResourceV1alpha2Interface
	SchedulingV1alpha1() schedulingv1alpha1.SchedulingV1alpha1Interface
	SchedulingV1beta1() schedulingv1beta1.SchedulingV1beta1Interface
	SchedulingV1() schedulingv1.SchedulingV1Interface
	StorageV1beta1() storagev1beta1.StorageV1beta1Interface
	StorageV1() storagev1.StorageV1Interface
	StorageV1alpha1() storagev1alpha1.StorageV1alpha1Interface
	StoragemigrationV1alpha1() storagemigrationv1alpha1.StoragemigrationV1alpha1Interface
}
```

### k8s 客户端

如何初始化一个 k8s clients(Clientset), 封装一个自己的client对象

```go
// 	"k8s.io/client-go/kubernetes" NewForConfig ---> Clientset
func NewForConfig(c *rest.Config) (*Clientset, error)
```

怎么获取的 *rest.Config("k8s.io/client-go/rest"):
+ 选择自己New
+ 通过 KubeConfig --> rest.Config (clientcmd.BuildConfigFromKubeconfigGetter)
```go
func NewClient(kubeConfigYaml string) (*Client, error) {
	// 1. 加载kubeconfig配置 文件内容 <---> KubeConfigd对象
    // 	"k8s.io/client-go/tools/clientcmd"
	kubeConf, err := clientcmd.Load([]byte(kubeConfigYaml))
	if err != nil {
		return nil, err
	}

	// 构造Restclient Config
    // KubeConfig ---> rest.Config
	restConf, err := clientcmd.BuildConfigFromKubeconfigGetter("",
		func() (*clientcmdapi.Config, error) {
			return kubeConf, nil
		},
	)
	if err != nil {
		return nil, err
	}

	// 初始化客户端
    // rest.Config ---> Clientset
	client, err := kubernetes.NewForConfig(restConf)
	if err != nil {
		return nil, err
	}

    // Ping
	items, err := client.ServerPreferredResources()
	if err != nil {
		return nil, err
	}

	return &Client{
		kubeconf:  kubeConf,
		restconf:  restConf,
		client:    client,
		resources: meta.NewApiResourceList(items),
		log:       log.Sub("provider.k8s"),
	}, nil
}
```

https://github.com/infraboard/mpaas: v0.0.20

```go
import (
	"testing"

	"github.com/infraboard/mpaas/provider/k8s"
	"github.com/infraboard/mpaas/test/tools"
)

var (
	client *k8s.Client
)

func TestServerVersion(t *testing.T) {
	v, err := client.ServerVersion()
	if err != nil {
		t.Fatal(err)
	}
	t.Log(v)
}

func TestServerResources(t *testing.T) {
	rs := client.ServerResources()

	for i := range rs.Items {
		item := rs.Items[i]
		t.Log(item.GroupVersion, item.APIVersion)
		for _, r := range item.APIResources {
			t.Log(r)
		}
	}
}

func init() {
	kubeConf := tools.MustReadContentFile("kube_config.yml")
	c, err := k8s.NewClient(kubeConf)
	if err != nil {
		panic(err)
	}
	client = c
}
```

## 基于client-go 操作Deploy

```go
// kubectl get deploy
c.client.AppsV1().Deployments("xxx").List()
```

```go
package workload_test

import (
	"fmt"
	"testing"

	"github.com/alibabacloud-go/tea/tea"
	"github.com/infraboard/mpaas/provider/k8s"
	"github.com/infraboard/mpaas/provider/k8s/meta"
	v1 "k8s.io/api/apps/v1"
	corev1 "k8s.io/api/core/v1"
	"k8s.io/apimachinery/pkg/api/resource"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"sigs.k8s.io/yaml"
)

func TestListDeployment(t *testing.T) {
	req := meta.NewListRequest()
	req.Namespace = "go8"
	v, err := impl.ListDeployment(ctx, req)
	if err != nil {
		t.Log(err)
	}
	for i := range v.Items {
		item := v.Items[i]
		t.Log(item.Namespace, item.Name)
	}
}

func TestGetDeployment(t *testing.T) {
	req := meta.NewGetRequest("coredns")
	req.Namespace = "kube-system"
	v, err := impl.GetDeployment(ctx, req)
	if err != nil {
		t.Log(err)
	}

	// 序列化
	yd, err := yaml.Marshal(v)
	if err != nil {
		t.Log(err)
	}
	t.Log(string(yd))
}

func TestCreateDeployment(t *testing.T) {
	req := &v1.Deployment{
		ObjectMeta: metav1.ObjectMeta{
			Name:      "nginx",
			Namespace: "default",
		},
		Spec: v1.DeploymentSpec{
			Replicas: tea.Int32(2),
			Selector: &metav1.LabelSelector{
				MatchLabels: map[string]string{"k8s-app": "nginx"},
			},
			Strategy: v1.DeploymentStrategy{
				Type: v1.RollingUpdateDeploymentStrategyType,
				RollingUpdate: &v1.RollingUpdateDeployment{
					MaxSurge:       k8s.NewIntStr(1),
					MaxUnavailable: k8s.NewIntStr(0),
				},
			},
			// Pod模板参数
			Template: corev1.PodTemplateSpec{
				ObjectMeta: metav1.ObjectMeta{
					Annotations: map[string]string{},
					Labels: map[string]string{
						"k8s-app": "nginx",
					},
				},
				Spec: corev1.PodSpec{
					// Pod参数
					DNSPolicy:                     corev1.DNSClusterFirst,
					RestartPolicy:                 corev1.RestartPolicyAlways,
					SchedulerName:                 "default-scheduler",
					TerminationGracePeriodSeconds: tea.Int64(30),
					// Container参数
					Containers: []corev1.Container{
						{
							Name:            "nginx",
							Image:           "nginx:latest",
							ImagePullPolicy: corev1.PullAlways,
							Env: []corev1.EnvVar{
								{Name: "APP_NAME", Value: "nginx"},
								{Name: "APP_VERSION", Value: "v1"},
							},
							Resources: corev1.ResourceRequirements{
								Limits: corev1.ResourceList{
									corev1.ResourceCPU:    resource.MustParse("500m"),
									corev1.ResourceMemory: resource.MustParse("1Gi"),
								},
								Requests: corev1.ResourceList{
									corev1.ResourceCPU:    resource.MustParse("50m"),
									corev1.ResourceMemory: resource.MustParse("50Mi"),
								},
							},
						},
					},
				},
			},
		},
	}

	yamlReq, err := yaml.Marshal(req)
	if err != nil {
		t.Fatal(err)
	}

	fmt.Println(string(yamlReq))

	d, err := impl.CreateDeployment(ctx, req)
	if err != nil {
		t.Log(err)
	}
	t.Log(d)
}

func TestScaleDeployment(t *testing.T) {
	req := meta.NewScaleRequest()
	req.Scale.Namespace = "default"
	req.Scale.Name = "nginx"
	req.Scale.Spec.Replicas = 1
	v, err := impl.ScaleDeployment(ctx, req)
	if err != nil {
		t.Fatal(err)
	}

	// 序列化
	yd, err := yaml.Marshal(v)
	if err != nil {
		t.Fatal(err)
	}
	t.Log(string(yd))
}

func TestReDeployment(t *testing.T) {
	req := meta.NewGetRequest("nginx")
	v, err := impl.ReDeploy(ctx, req)
	if err != nil {
		t.Fatal(err)
	}

	// 序列化
	yd, err := yaml.Marshal(v)
	if err != nil {
		t.Fatal(err)
	}
	t.Log(string(yd))
}

func TestDeleteDeployment(t *testing.T) {
	req := meta.NewDeleteRequest("nginx")
	err := impl.DeleteDeployment(ctx, req)
	if err != nil {
		t.Fatal(err)
	}
}
```


## 基于 client-go 查看Pod日志

```go
import (
	"testing"

	"github.com/infraboard/mpaas/provider/k8s/meta"
	"github.com/infraboard/mpaas/provider/k8s/workload"
	"github.com/infraboard/mpaas/test/tools"
	batchv1 "k8s.io/api/batch/v1"
	v1 "k8s.io/api/core/v1"
	"sigs.k8s.io/yaml"
)

func TestListPod(t *testing.T) {
	req := meta.NewListRequest()
	req.Namespace = "default"
	// req.Opts.LabelSelector = "job-name=test-job"
	pods, err := impl.ListPod(ctx, req)
	if err != nil {
		t.Fatal(err)
	}

	// 序列化
	for _, v := range pods.Items {
		t.Log(v.Namespace, v.Name)
	}
}

func TestGetPod(t *testing.T) {
	req := meta.NewGetRequest("kubernetes-proxy-78d4f87b58-crmlm")

	pods, err := impl.GetPod(ctx, req)
	if err != nil {
		t.Fatal(err)
	}

	// 序列化
	yd, err := yaml.Marshal(pods)
	if err != nil {
		t.Fatal(err)
	}
	t.Log(string(yd))
}
```


```go
func TestWatchConainterLog(t *testing.T) {
	req := workload.NewWatchConainterLogRequest()
	req.Follow = false
	req.Namespace = "default"
	req.PodName = "nginx-974d7fcf-z7c8x"
	stream, err := impl.WatchConainterLog(ctx, req)
	if err != nil {
		t.Fatal(err)
	}
	defer stream.Close()
	_, err = io.Copy(os.Stdout, stream)
	if err != nil {
		t.Fatal(err)
	}
}
```

## 基于 client-go 的 webtermianl


1. remotecommand --->  stream(io) ----> terminal
```go
// 登录容器
func (c *Client) LoginContainer(ctx context.Context, req *LoginContainerRequest) error {
    // RESTClient()
    // SDK List/Delete/Update
    // corev1.RESTClient() ---> 构建一个 Restulf Request --> POST /pods/{podName}/namespace/exec
	restReq := c.corev1.RESTClient().Post().
		Resource("pods").
		Name(req.PodName).
		Namespace(req.Namespace).
		SubResource("exec")

	restReq.VersionedParams(&v1.PodExecOptions{
		Container: req.ContainerName,
		Command:   req.Command,
		Stdin:     true,
		Stdout:    true,
		Stderr:    true,
		TTY:       true,
	}, scheme.ParameterCodec)

    //  remotecommand.NewSPDYExecutor --> api server  websocket(io stream) excutor
    // executor(pod) <---- input
    // excutor(pod)  ----> output
	executor, err := remotecommand.NewSPDYExecutor(c.restconf, "POST", restReq.URL())
	if err != nil {
		return err
	}

	return executor.StreamWithContext(ctx, remotecommand.StreamOptions{
		Stdin:             req.Executor,
		Stdout:            req.Executor,
		Stderr:            req.Executor,
		Tty:               true,
		TerminalSizeQueue: req.Executor,
	})
}
```


```go
// StreamOptions holds information pertaining to the current streaming session:
// input/output streams, if the client is requesting a TTY, and a terminal size queue to
// support terminal resizing.
type StreamOptions struct {
	Stdin             io.Reader
	Stdout            io.Writer
	Stderr            io.Writer
	Tty               bool
	TerminalSizeQueue TerminalSizeQueue
}
```


```go
func (h *websocketHandler) registryPodHandler(ws *restful.WebService) {
	tags := []string{"[Proxy] Pod管理"}

	ws.Route(ws.GET("/{cluster_id}/{namespace}/pods/{name}/login").To(h.LoginContainer).
		Doc("登陆Pod").
		Metadata(restfulspec.KeyOpenAPITags, tags).
		Metadata(label.Resource, "pod_terminal").
		Metadata(label.Action, label.Create.Value()).
		Reads(cluster.QueryClusterRequest{}).
		Writes(corev1.Pod{}).
		Returns(200, "OK", corev1.Pod{}))

	ws.Route(ws.GET("/{cluster_id}/{namespace}/pods/{name}/log").To(h.WatchConainterLog).
		Doc("查看Pod日志").
		Metadata(restfulspec.KeyOpenAPITags, tags).
		Metadata(label.Resource, "pod_terminal").
		Metadata(label.Action, label.Get.Value()).
		Reads(cluster.QueryClusterRequest{}).
		Writes(corev1.Pod{}).
		Returns(200, "OK", corev1.Pod{}))
}
```

![alt text](image.png)

## k8s operator 原理介绍 与实践


## mflow 设计



## 使用k8s job的流水线设计方案解读


## Docker Build Job执行演示