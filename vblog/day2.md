# 用户管理模块开发

+ 用户管理模块的业务定义(面向接口)
+ 如何管理项目的配置
+ 用户管理模块接口实现与测试
+ 基于Bcrypt处理用户密码存储问题

详细说明:
+ 面向接口与业务分区模型下的应用实践
+ 使用接口来定义用户管理模块的业务领域接口
+ 枚举对象的应用场景以及如何定义
+ 如何设计程序的配置对象
+ 如何基于配置对象构造单例模式的数据库连接池对象(GROM DB对象)
+ 业务控制器如何处理对配置对象的依赖
+ 配置对象的加载(支持环境变量与配置文件)
+ 为配置对象编写单元测试
+ 设计用户管理模块的业务领域接口的具体实现
+ 为领域接口的实现 编写单元测试
+ 领域接口的实现类(具体实现) 实现用户创建方法
+ 使用单元测试验证实现类实现的用户创建方法是否正确