# k8s operator

[kubebuilder 官方文档](https://book.kubebuilder.io/quick-start)

```sh
kubebuilder init --domain go15 --repo gitlab.com/go-course-project/go15/skills/operator/myoperator
```


## Makefile

```sh
make help

Usage:
  make <target>

General
  help             Display this help.

Development
  manifests        Generate WebhookConfiguration, ClusterRole and CustomResourceDefinition objects.
  generate         Generate code containing DeepCopy, DeepCopyInto, and DeepCopyObject method implementations.
  fmt              Run go fmt against code.
  vet              Run go vet against code.
  test             Run tests.
  lint             Run golangci-lint linter
  lint-fix         Run golangci-lint linter and perform fixes

Build
  build            Build manager binary.
  run              Run a controller from your host.
  docker-build     Build docker image with the manager.
  docker-push      Push docker image with the manager.
  docker-buildx    Build and push docker image for the manager for cross-platform support
  build-installer  Generate a consolidated YAML with CRDs and deployment.

Deployment
  install          Install CRDs into the K8s cluster specified in ~/.kube/config.
  uninstall        Uninstall CRDs from the K8s cluster specified in ~/.kube/config. Call with ignore-not-found=true to ignore resource not found errors during deletion.
  deploy           Deploy controller to the K8s cluster specified in ~/.kube/config.
  undeploy         Undeploy controller from the K8s cluster specified in ~/.kube/config. Call with ignore-not-found=true to ignore resource not found errors during deletion.

Dependencies
  kustomize        Download kustomize locally if necessary.
  controller-gen   Download controller-gen locally if necessary.
  envtest          Download setup-envtest locally if necessary.
  golangci-lint    Download golangci-lint locally if necessary.
```

## CRD API

```sh
kubebuilder create api -h
Scaffold a Kubernetes API by writing a Resource definition and/or a Controller.

If information about whether the resource and controller should be scaffolded
was not explicitly provided, it will prompt the user if they should be.

After the scaffold is written, the dependencies will be updated and
make generate will be run.

Usage:
  kubebuilder create api [flags]

Examples:
  # Create a frigates API with Group: ship, Version: v1beta1 and Kind: Frigate
  kubebuilder create api --group ship --version v1beta1 --kind Frigate

  # Edit the API Scheme

  nano api/v1beta1/frigate_types.go

  # Edit the Controller
  nano internal/controller/frigate/frigate_controller.go

  # Edit the Controller Test
  nano internal/controller/frigate/frigate_controller_test.go

  # Generate the manifests
  make manifests

  # Install CRDs into the Kubernetes cluster using kubectl apply
  make install

  # Regenerate code and run against the Kubernetes cluster configured by ~/.kube/config
  make run


Flags:
      --controller           if set, generate the controller without prompting the user (default true)
      --force                attempt to create resource even if it already exists
      --group string         resource Group
  -h, --help                 help for api
      --kind string          resource Kind
      --make make generate   if true, run make generate after generating files (default true)
      --namespaced           resource is namespaced (default true)
      --plural string        resource irregular plural form
      --resource             if set, generate the resource without prompting the user (default true)
      --version string       resource Version

Global Flags:
      --plugins strings   plugin keys to be used for this subcommand execution
```

```sh
kubebuilder create api --group host --version v1beta1 --kind VirtrualMachine
INFO Create Resource [y/n]                        
y
INFO Create Controller [y/n]                      
y
INFO Writing kustomize manifests for you to edit... 
INFO Writing scaffold for you to edit...          
INFO api/v1beta1/virtrualmachine_types.go         
INFO api/v1beta1/groupversion_info.go             
INFO internal/controller/suite_test.go            
INFO internal/controller/virtrualmachine_controller.go 
INFO internal/controller/virtrualmachine_controller_test.go 
INFO Update dependencies:
$ go mod tidy           
INFO Running make:
$ make generate                
/Users/yumaojun/Projects/go-course/go15/skills/operator/myoperator/bin/controller-gen object:headerFile="hack/boilerplate.go.txt" paths="./..."
Next: implement your new API and generate the manifests (e.g. CRDs,CRs) with:
$ make manifests
```

## Resource Define

```go
// VirtrualMachine is the Schema for the virtrualmachines API
type VirtrualMachine struct {
	metav1.TypeMeta   `json:",inline"`
	metav1.ObjectMeta `json:"metadata,omitempty"`

	Spec   VirtrualMachineSpec   `json:"spec,omitempty"`
	Status VirtrualMachineStatus `json:"status,omitempty"`
}
```

```go
// VirtrualMachineSpec defines the desired state of VirtrualMachine
type VirtrualMachineSpec struct {
	// INSERT ADDITIONAL SPEC FIELDS - desired state of cluster
	// Important: Run "make" to regenerate code after modifying this file

	// KVM
	// 资源地域
	Region string `json:"region"`
	// 可用区
	Zone string `json:"zone"`
	// Cpu
	Cpu int `json:"cpu"`
	// 内存
	Memory int `json:"memory"`
	//
	Storage int `json:"storage"`
}

// VirtrualMachineStatus defines the observed state of VirtrualMachine
type VirtrualMachineStatus struct {
	// INSERT ADDITIONAL STATUS FIELD - define observed state of cluster
	// Important: Run "make" to regenerate code after modifying this file
	// Ip地址
	Ip string `json:"storage"`
	//
	Stage Stage `json:"stage"`
}

type Stage string

const (
	STAGE_PADDING        = "padding"
	STAGE_CREATING       = "creating"
	STAGE_CREATE_FAILED  = "create_failed"
	STAGE_CREATE_RUNNING = "create_failed"
)
```

```sh
➜  myoperator git:(main) ✗ make manifests
/Users/yumaojun/Projects/go-course/go15/skills/operator/myoperator/bin/controller-gen rbac:roleName=manager-role crd webhook paths="./..." output:crd:artifacts:config=config/crd/bases
➜  myoperator git:(main) ✗ make generate
/Users/yumaojun/Projects/go-course/go15/skills/operator/myoperator/bin/controller-gen object:headerFile="hack/boilerplate.go.txt" paths="./..."
```

## CRD 安装

```sh
kubectl get crd    
No resources found
```

```sh
➜  myoperator git:(main) ✗ kubectl get crd                                   
NAME                         CREATED AT
virtrualmachines.host.go15   2024-09-21T08:47:26Z
```

创建一个自定义资源

```sh
$ kubectl apply -f config/samples/host_v1beta1_virtrualmachine.yaml 
virtrualmachine.host.go15/virtrualmachine-sample created

$ kubectl get VirtrualMachine virtrualmachine-sample -o yaml
apiVersion: host.go15/v1beta1
kind: VirtrualMachine
metadata:
  annotations:
    kubectl.kubernetes.io/last-applied-configuration: |
      {"apiVersion":"host.go15/v1beta1","kind":"VirtrualMachine","metadata":{"annotations":{},"labels":{"app.kubernetes.io/managed-by":"kustomize","app.kubernetes.io/name":"myoperator"},"name":"virtrualmachine-sample","namespace":"default"},"spec":{"cpu":4,"memory":8,"region":"hangzhou","storage":50,"zone":"01"}}
  creationTimestamp: "2024-09-21T08:52:26Z"
  generation: 1
  labels:
    app.kubernetes.io/managed-by: kustomize
    app.kubernetes.io/name: myoperator
  name: virtrualmachine-sample
  namespace: default
  resourceVersion: "841932"
  uid: d38c73bd-283d-4483-9e18-301b0d4b061d
spec:
  cpu: 4
  memory: 8
  region: hangzhou
  storage: 50
  zone: "01"
```


## 实现Controller

```go
func (r *VirtrualMachineReconciler) Reconcile(ctx context.Context, req ctrl.Request) (ctrl.Result, error) {
	l := log.FromContext(ctx, "namespace", req.Namespace)

	// TODO(user): your logic here
	// 获取当前这个对象
	obj := &v1beta1.VirtrualMachine{}
	if err := r.Get(ctx, req.NamespacedName, obj); err != nil {
		if apierrors.IsNotFound(err) {
			// 说明这个资源已经被删除
			l.Info("%s not found", req.NamespacedName)
		}
		return ctrl.Result{}, nil
	}

	// 判断状态有没有达到预期
	// 1. 达到预期，直接忽略
	l.Info("获取到资源", "name", obj.Name)

	switch obj.Status.Stage {
	case hostv1beta1.STAGE_CREATE_FAILED, hostv1beta1.STAGE_RUNNING:
		return ctrl.Result{}, nil
	case hostv1beta1.STAGE_CREATING:
		// 同步一下状态, 看有没有创建完成, 没完成5秒后再重试
		return ctrl.Result{Requeue: true, RequeueAfter: 5 * time.Second}, nil
	case hostv1beta1.STAGE_PADDING, "":
		// 2. 没达到预期--> 掉API, 等。。。 达到预期
		// 处理完成功, 更新Api Object的状态
		l.Info("%s 调用KVM API 进行资源创建中 ...", "name", obj.Name)

		obj.Status.Stage = hostv1beta1.STAGE_RUNNING
		if err := r.Update(ctx, obj); err != nil {
			l.Error(err, "更新对象状态失败")
		}
	default:
		return ctrl.Result{}, apierrors.NewBadRequest("状态未识别: " + string(obj.Status.Stage))
	}

	return ctrl.Result{}, nil
}
```


```sh
/Users/yumaojun/Projects/go-course/go15/skills/operator/myoperator/bin/controller-gen rbac:roleName=manager-role crd webhook paths="./..." output:crd:artifacts:config=config/crd/bases
/Users/yumaojun/Projects/go-course/go15/skills/operator/myoperator/bin/controller-gen object:headerFile="hack/boilerplate.go.txt" paths="./..."
go fmt ./...
go vet ./...
go run ./cmd/main.go
2024-09-21T18:02:18+08:00       INFO    setup   starting manager
2024-09-21T18:02:18+08:00       INFO    starting server {"name": "health probe", "addr": "[::]:8081"}
2024-09-21T18:02:18+08:00       INFO    Starting EventSource    {"controller": "virtrualmachine", "controllerGroup": "host.go15", "controllerKind": "VirtrualMachine", "source": "kind source: *v1beta1.VirtrualMachine"}
2024-09-21T18:02:18+08:00       INFO    Starting Controller     {"controller": "virtrualmachine", "controllerGroup": "host.go15", "controllerKind": "VirtrualMachine"}
2024-09-21T18:02:18+08:00       INFO    Starting workers        {"controller": "virtrualmachine", "controllerGroup": "host.go15", "controllerKind": "VirtrualMachine", "worker count": 1}
2024-09-21T18:02:18+08:00       INFO    获取到资源      {"controller": "virtrualmachine", "controllerGroup": "host.go15", "controllerKind": "VirtrualMachine", "VirtrualMachine": {"name":"virtrualmachine-sample","namespace":"default"}, "namespace": "default", "name": "virtrualmachine-sample", "reconcileID": "3774465c-29c3-4057-85f4-925110d8b8f4", "namespace": "default", "name": "virtrualmachine-sample"}
2024-09-21T18:02:18+08:00       INFO    %s 调用KVM API 进行资源创建中 ...       {"controller": "virtrualmachine", "controllerGroup": "host.go15", "controllerKind": "VirtrualMachine", "VirtrualMachine": {"name":"virtrualmachine-sample","namespace":"default"}, "namespace": "default", "name": "virtrualmachine-sample", "reconcileID": "3774465c-29c3-4057-85f4-925110d8b8f4", "namespace": "default", "name": "virtrualmachine-sample"}
```