/*
Copyright 2024.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package v1beta1

import (
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
)

// EDIT THIS FILE!  THIS IS SCAFFOLDING FOR YOU TO OWN!
// NOTE: json tags are required.  Any new fields you add must have json tags for the fields to be serialized.

// VirtrualMachineSpec defines the desired state of VirtrualMachine
type VirtrualMachineSpec struct {
	// INSERT ADDITIONAL SPEC FIELDS - desired state of cluster
	// Important: Run "make" to regenerate code after modifying this file

	// KVM
	// 资源地域
	Region string `json:"region"`
	// 可用区
	Zone string `json:"zone"`
	// Cpu
	Cpu int `json:"cpu"`
	// 内存
	Memory int `json:"memory"`
	//
	Storage int `json:"storage"`
}

// VirtrualMachineStatus defines the observed state of VirtrualMachine
type VirtrualMachineStatus struct {
	// INSERT ADDITIONAL STATUS FIELD - define observed state of cluster
	// Important: Run "make" to regenerate code after modifying this file
	// Ip地址
	Ip string `json:"storage"`
	//
	Stage Stage `json:"stage"`
}

type Stage string

const (
	STAGE_PADDING       = "padding"
	STAGE_CREATING      = "creating"
	STAGE_CREATE_FAILED = "create_failed"
	STAGE_RUNNING       = "running"
)

// +kubebuilder:object:root=true
// +kubebuilder:subresource:status

// VirtrualMachine is the Schema for the virtrualmachines API
type VirtrualMachine struct {
	metav1.TypeMeta   `json:",inline"`
	metav1.ObjectMeta `json:"metadata,omitempty"`

	Spec   VirtrualMachineSpec   `json:"spec,omitempty"`
	Status VirtrualMachineStatus `json:"status,omitempty"`
}

// +kubebuilder:object:root=true

// VirtrualMachineList contains a list of VirtrualMachine
type VirtrualMachineList struct {
	metav1.TypeMeta `json:",inline"`
	metav1.ListMeta `json:"metadata,omitempty"`
	Items           []VirtrualMachine `json:"items"`
}

func init() {
	SchemeBuilder.Register(&VirtrualMachine{}, &VirtrualMachineList{})
}
