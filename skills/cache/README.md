# 缓存

缓存: https://gitee.com/infraboard/go-course/tree/master/extra/redis

```python
# fn = wrapper(fn)
# GetObject = cache(GetObject)
# GetObject = warpper(Get(ctx, GetObject))
@cache(ttl=300)
def GetObject() {

}
```

## 环境

```sh
# 使用Docker启动一个Redis服务
docker run -p 6379:6379 -itd --name redis  redis
# 进入Redis的命令交互界面
docker exec -it redis redis-cli
```


## sdk

https://redis.uptrace.dev/zh/guide/go-redis.html#dial-tcp-i-o-timeout

``` sh
// 这个逻辑比较号时, 请求结果缓存 30s
// 缓存命中 id ---> redis.get(”)   json --> unmarshal --> return obj
// 缓存未命中 id ---> redis.get(”)  DescribeSecret() --> obj ----> redis.set(obj) ----> return obj
// 选择一个redis 库 https://redis.uptrace.dev/zh/guide/go-redis.html#dial-tcp-i-o-timeout
```

```go
// 连接数据库
rdb := redis.NewClient(&redis.Options{
	Addr:	  "localhost:6379",
	Password: "", // 没有密码，默认值
	DB:		  0,  // 默认DB 0
})

// 设置值
val, err := rdb.Get(ctx, "key").Result()
fmt.Println(val)

// 获取值
get := rdb.Get(ctx, "key")
fmt.Println(get.Val(), get.Err())
```

## Rdids集成使用

```go
[redis]
  endpoints = ["127.0.0.1:6379"]
  db = 0
  username = ""
  password = ""
  enable_trace = true
```

```go
package main

import (
	"fmt"

	"github.com/infraboard/mcube/v2/ioc/config/redis"
)

func main() {
	client := redis.Client()
	fmt.Println(client)
}
```

## Cache模块的集成

```toml
[cache]
  # 使用换成提供方, 默认使用GoCache提供的内存缓存, 如果配置为redis 还需要配置redis的配置
  provider = "redis"
  # 单位秒, 默认5分钟
  ttl = 300
```

将缓存逻辑封装在内部
```go
package main

import (
	"context"

	"github.com/infraboard/mcube/v2/ioc/config/cache"
)

type TestStruct struct {
	Id     string
	FiledA string
}

func main() {
	ctx := context.Background()

	var v *TestStruct

	// objectId --->  cached Objected
	// 获取objectId对应的对象, 如果缓存中有则之间从缓存中获取, 如果没有 则通过提供的ObjectFinder直接获取
	err := cache.NewGetter(ctx, func(ctx context.Context, objectId string) (any, error) {
		return &TestStruct{Id: objectId, FiledA: "test"}, nil
	}).Get("objectId", v)
	if err != nil {
		panic(err)
	}
}
```