package api

import (
	"github.com/emicklei/go-restful/v3"
	"github.com/infraboard/mcube/v2/http/request"
	"github.com/infraboard/mcube/v2/http/restful/response"
	"gitlab.com/go-course-project/go15/devcloud-mini/maudit/apps/event"
)

// RouteFunction declares the signature of a function that can be bound to a Route.
// type RouteFunction func(*Request, *Response)

// 事件查询的接口给到UI
// GoRestful
func (h *handler) QueryEvent(r *restful.Request, w *restful.Response) {
	req := event.NewQueryEventRequest()
	req.PageRequest = request.NewPageRequestFromHTTP(r.Request)
	// /{id}
	// r.PathParameter()
	// // /{id}?xxx=xxxx
	// r.QueryParameter()
	// //
	// req.PageRequest = &request.PageRequest{
	// 	PageSize: r.QueryParameter("page_"),
	// }

	set, err := h.svc.QueryEvent(r.Request.Context(), req)
	if err != nil {
		response.Failed(w, err)
		return
	}

	response.Success(w, set)
}
