# 中央化的鉴权系统


## 初始化

```go
// 导出初始化命令
"github.com/infraboard/mcenter/cmd/initial"

cmd.Root.AddCommand(initial.Cmd)
```

初始化系统默认用户与角色
```sh
初始化域:           default [成功]
初始化系统管理员:     admin [成功]

初始化空间:         default [成功]
初始化空间:          system [成功]

初始化角色:           admin [成功]
初始化角色:         visitor [成功]

初始化服务:           mflow [成功], client_id: cBPjWwoiTFc0ykp0Weg2IaAx, client_secret: fCU8D8Rhjsi95LI6haYnLOaHfB3NRcZl
初始化服务:       moperator [成功], client_id: cEnWgsq9yZFFaDMLe7OzP88P, client_secret: pebY7Dk3rLweBMpFH87jD9Z2rzDgFSe1
初始化服务:           mpaas [成功], client_id: Z8CnGRAYLaJTDYifIgQsBb3W, client_secret: HH8ljqgF9wv7KcOCX2D4YtFehP3RwJ20

初始化标签:             Env [成功]
初始化标签:   ResourceGroup [成功]
初始化标签:       UserGroup [成功]
初始化标签:     DeployGroup [成功]
```


## mcenter 自己怎么认证

他也需要一个中间件, 指向的认证token模块 就本地模块

自己需要注入的中间件
```go
_ "github.com/infraboard/mcenter/middlewares/endpoint"
_ "github.com/infraboard/mcenter/middlewares/grpc"
_ "github.com/infraboard/mcenter/middlewares/http"
```

## cmdb服务接入用户中心

1. 创建一个cmdb服务:
```json
{
  "description": "资源管理中心",
  "name": "cmdb",
  "type": "CONTAINER_IMAGE"
}
```

服务凭证
```json
{
    "id": "4fb38cbffae3315f",
    "create_at": 0,
    "update_at": 0,
    "update_by": "",
    "domain": "default",
    "namespace": "default",
    "owner": "admin@default",
    "enabled": true,
    "type": "CONTAINER_IMAGE",
    "name": "cmdb",
    "logo": "",
    "description": "资源管理中心",
    "level": 0,
    "code_repository": {
        "provider": "GITLAB",
        "token": "",
        "project_id": "",
        "namespace": "",
        "web_url": "",
        "ssh_url": "",
        "http_url": "",
        "language": null,
        "enable_hook": true,
        "hook_config": "",
        "hook_id": "",
        "created_at": 0
    },
    "image_repository": {
        "address": "",
        "version": "latest"
    },
    "labels": {},
    "credential": {
        "enabled": false,
        "update_at": 0,
        "client_id": "vwbCY3dSaROCjgeO6eniMKmn",
        "client_secret": "I1CTvzFRWCSOS8wilCBjhJlSEIz2w3O1"
    },
    "security": {
        "encrypt_key": "xMfUyCt4M69wN9RafZ6E2LzTsq6EXTmjMPJerbIvxzZ2JAr2o08zKtzFmcHBQax5"
    }
}
```

1. cmdb 服务接入mcenter

```go
// 接入用户中心
_ "gitlab.com/go-course-project/go15/devcloud-mini/mcenter/clients/middleware"
```

2. 添加中间件的配置:
```toml
[mcenter]
# grpc地址
address="127.0.0.1:18010"
client_id="vwbCY3dSaROCjgeO6eniMKmn"
client_secret="I1CTvzFRWCSOS8wilCBjhJlSEIz2w3O1"
```

3. API 开启鉴权
```go
// 开启认证
Metadata(label.Auth, label.Enable).
Metadata(label.Permission, label.Enable).
```

## 认证

```sh
curl --location 'http://127.0.0.1:8010/api/mcenter_go15/v1/token' \
--header 'Content-Type: application/json' \
--header 'Cookie: mcenter.access_token=0NZJXCu6YEJuNJJ7wqnP3Swz' \
--data '{
    "username": "admin",
    "password": "123456"
}'
```

```json
{
    "platform": "WEB",
    "access_token": "OFW6wvwKZqU7SBNC54zgMEXO",
    "refresh_token": "vEUUmxVtZbjxyimBsADMk6cp0jOKG9sc",
    "issue_at": 1725679396,
    "access_expired_at": 3600,
    "refresh_expired_at": 14400,
    "user_type": "SUPPER",
    "domain": "default",
    "username": "admin",
    "user_id": "admin@default",
    "shared_user": false,
    "grant_type": "PASSWORD",
    "type": "BEARER",
    "namespace": "default",
    "is_namespace_manager": false,
    "status": {
        "is_block": false,
        "block_type": "REFRESH_TOKEN_EXPIRED",
        "block_at": 0,
        "block_reason": ""
    },
    "location": {
        "ip_location": {
            "remote_ip": "127.0.0.1",
            "country": "0",
            "region": "0",
            "province": "0",
            "city": "内网IP",
            "isp": "内网IP"
        },
        "user_agent": {
            "os": "",
            "platform": "",
            "engine_name": "",
            "engine_version": "",
            "browser_name": "PostmanRuntime",
            "browser_version": "7.40.0"
        }
    },
    "meta": {
        "domain_description": "基础设施服务中心",
        "domain_logo_path": "",
        "reset_password": "由于安全及管理需要, 您必须重置您的登录密码之后, 才能访问系统"
    }
}
```

1. 调用cmdb接口: http://127.0.0.1:8020/api/cmdb/v1/secret
```json
{
"namespace": "cmdb",
"http_code": 401,
"code": 401,
"reason": "认证失败",
"message": "Auth Header Required, Format: Authorization: Bearer ${access_token}",
"meta": null,
"data": null
}
```

## 鉴权

1. 创建一个用户
```sh
curl --location 'http://127.0.0.1:8010/api/mcenter_go15/v1/user/sub' \
--header 'Content-Type: application/json' \
--header 'Cookie: mcenter.access_token=OFW6wvwKZqU7SBNC54zgMEXO' \
--data '{
   "username": "test",
   "password": "123456"
}'
```

2. 创建一个角色
```sh
curl --location 'http://127.0.0.1:8010/api/mcenter_go15/v1/role' \
--header 'Content-Type: application/json' \
--header 'Cookie: mcenter.access_token=OFW6wvwKZqU7SBNC54zgMEXO' \
--data '{
    "description": "cmdb服务访问测试",
    "name": "test",
    "permissions": [
        {
            "effect": "allow",
            "label_key": "action",
            "label_values": [
                "get"
            ],
            "resource_name": "*",
            "service_id": "*"
        }
    ]
}'
````

3. 在创建一个策略: 用户和角色绑定

```sh
curl --location 'http://127.0.0.1:8010/api/mcenter_go15/v1/policy' \
--header 'Content-Type: application/json' \
--header 'Cookie: mcenter.access_token=OFW6wvwKZqU7SBNC54zgMEXO' \
--data-raw '{
    "enabled": true,
    "namespace": "default",
    "role_id": "fbfbe567ed205fc8",
    "user_id": "test@default"
}'
```

4. 登录 test 角色 进行验证

```sh
curl --location 'http://127.0.0.1:8010/api/mcenter_go15/v1/token' \
--header 'Content-Type: application/json' \
--header 'Cookie: mcenter.access_token=ZB5gWmgsDOwT9OFSorzWY8LF' \
--data '{
    "username": "test",
    "password": "123456"
}'
```

```json
{
    "platform": "WEB",
    "access_token": "ZB5gWmgsDOwT9OFSorzWY8LF",
    "refresh_token": "y4vXc5nVel8Dt0JiZRQxn6BYtOjp4FYn",
    "issue_at": 1725693158,
    "access_expired_at": 3600,
    "refresh_expired_at": 14400,
    "user_type": "SUB",
    "domain": "default",
    "username": "test",
    "user_id": "test@default",
    "shared_user": false,
    "grant_type": "PASSWORD",
    "type": "BEARER",
    "namespace": "default",
    "is_namespace_manager": false,
    "status": {
        "is_block": false,
        "block_type": "REFRESH_TOKEN_EXPIRED",
        "block_at": 0,
        "block_reason": ""
    },
    "location": {
        "ip_location": {
            "remote_ip": "127.0.0.1",
            "country": "0",
            "region": "0",
            "province": "0",
            "city": "内网IP",
            "isp": "内网IP"
        },
        "user_agent": {
            "os": "",
            "platform": "",
            "engine_name": "",
            "engine_version": "",
            "browser_name": "PostmanRuntime",
            "browser_version": "7.40.0"
        }
    },
    "meta": {
        "domain_description": "基础设施服务中心",
        "domain_logo_path": "",
        "reset_password": "由于安全及管理需要, 您必须重置您的登录密码之后, 才能访问系统"
    }
}
```

get 接口正常
```sh
curl --location 'http://127.0.0.1:8020/api/cmdb/v1/secret/05fdac51-cb80-32ee-996a-ef7a781dd392' \
--header 'Cookie: mcenter.access_token=ZB5gWmgsDOwT9OFSorzWY8LF'
```

list 接口没权限
```sh
curl --location 'http://127.0.0.1:8020/api/cmdb/v1/secret' \
--header 'Cookie: mcenter.access_token=ZB5gWmgsDOwT9OFSorzWY8LF'
```

```json
{
    "namespace": "cmdb",
    "http_code": 403,
    "code": 403,
    "reason": "访问未授权",
    "message": "你当前角色([test@default])在空间(default)中无权限对secret资源进行QuerySecret操作",
    "meta": null,
    "data": null
}
```


## 飞书三方登录

```sh
https://petstore.swagger.io/?code=177ka2fddb3b478b8f2d0143a4737cc1&state=STATE

auth_code: 56cjfae9bbac44929079683876a845b4
```


```json
{"access_token":"f4PYZoaCB33aUhiw7YirZLg115Ewl5fzMW0054Kw283h","refresh_token":"fTEs1OSdN0qrl4uKGaRqX6g13dwwl57Fia00h0Ww2dil","token_type":"Bearer","expires_in":7200,"refresh_expires_in":2592000,"scope":"auth:user.id:read"}
```


```sh
curl --location 'http://127.0.0.1:8010/api/mcenter_go15/v1/oauth2/feishu/?code=837j3671c46b4d0a8ddf19913f044f19&state=STATE' \
--header 'Cookie: mcenter.access_token=xJV0MEOT8wUE4xWqvejdOPQ5'
```