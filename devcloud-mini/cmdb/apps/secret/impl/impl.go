package impl

import (
	"github.com/infraboard/mcube/v2/ioc"
	"github.com/infraboard/mcube/v2/ioc/config/log"
	"github.com/rs/zerolog"
	"gitlab.com/go-course-project/go15/devcloud-mini/cmdb/apps/secret"

	ioc_mongo "github.com/infraboard/mcube/v2/ioc/config/mongo"
	"github.com/infraboard/mcube/v2/ioc/config/redis"
	go_redis "github.com/redis/go-redis/v9"
	"go.mongodb.org/mongo-driver/mongo"
)

func init() {
	ioc.Controller().Registry(&impl{})
}

var _ secret.Service = (*impl)(nil)

// 业务具体实现
type impl struct {
	// 继承模版
	ioc.ObjectImpl

	// 模块子Logger
	log *zerolog.Logger

	//
	col *mongo.Collection

	// redis client
	redis go_redis.UniversalClient
}

// 对象名称
func (i *impl) Name() string {
	return secret.AppName
}

// 初始化
func (i *impl) Init() error {
	// 对象
	i.log = log.Sub(i.Name())
	i.log.Debug().Msgf(ioc_mongo.Get().Database)
	i.col = ioc_mongo.DB().Collection("secrets")

	// redis对象
	i.redis = redis.Client()
	return nil
}
