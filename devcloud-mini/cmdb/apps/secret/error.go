package secret

import "github.com/infraboard/mcube/v2/exception"

const (
	CODE_INSUFFICIENT_BALANCE = 100001
)

func NewErrInsufficientBalance() *exception.ApiException {
	return exception.NewApiException(CODE_INSUFFICIENT_BALANCE, "余额不足")
}

func IsErrInsufficientBalance(err error) bool {
	return exception.IsApiException(err, CODE_INSUFFICIENT_BALANCE)
}
