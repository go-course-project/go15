package main

import (
	"github.com/infraboard/mcube/v2/ioc/server/cmd"

	_ "gitlab.com/go-course-project/go15/devcloud-mini/cmdb/apps"

	// 开启API Doc
	// docker run -p 80:8080 swaggerapi/swagger-ui
	_ "github.com/infraboard/mcube/v2/ioc/apps/apidoc/restful"
	// 引入CORS组件
	_ "github.com/infraboard/mcube/v2/ioc/config/cors/gorestful"

	// 接入审计中心
	_ "gitlab.com/go-course-project/go15/devcloud-mini/maudit/clients/middleware"

	// 接入用户中心
	_ "gitlab.com/go-course-project/go15/devcloud-mini/mcenter/clients/middleware"
)

func main() {
	cmd.Start()
}
